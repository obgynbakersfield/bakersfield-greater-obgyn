**Bakersfield greater obgyn**

From your first prenatal visit to your postpartum time, our Greater Bakersfield OBGYN delivers extensive obstetric treatment. 
We also have laboratory services on site and ultrasound on site. 
For obstetric treatment, we have three physicians available.
Although you will see one specialist for the bulk of your birth, you will still see the others in our clinic so that you are 
acquainted with them all and happy with them.
Please Visit Our Website [Bakersfield greater obgyn](https://obgynbakersfield.com/greater-obgyn.php) for more information.
---

## Our greater obgyn in Bakersfield 

A state-of-the-art Labor and Delivery Center, recently renovated in 2019, is provided by the Greater Bakersfield OBGYN. 
Our office location right behind the hospital means we are readily available during childbirth for all our patients at a moment's notice. 
If you have any more concerns or would like to make a consultation, please feel free to email us.
